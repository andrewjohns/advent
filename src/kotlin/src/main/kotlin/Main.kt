import java.io.File

fun main(args: Array<String>) {
    val puzzle = twelve.Puzzle()
    val puzzleNumber = "twelve"
    val file = File("src/main/resources/${puzzleNumber}/data.txt")
    puzzle.playGame(file, 5)
}