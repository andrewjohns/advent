package eight

import java.io.File

data class Ghost(
    var position: Coordinate,
    var stepsToZ: Int = 0
)

data class Coordinate(
    val coordinate: String,
    val left: String,
    val right: String
)

class Puzzle {
    private var instructions: String = ""
    private var data: List<String> = listOf()
    private var map = HashMap<String, List<String>>()
    private var currentPosition: String = "AAA"
    private var goal: String = "ZZZ"

    fun setGame(gameFile : File) {
        this.data = gameFile.readLines()
        this.instructions = this.data.get(0)
        data.slice(2..<data.size).forEach { line ->
            val step = line.split('=')
            val coordinate = step[0].trim()
            val leftRight = step[1].trim().replace("(", "").replace(")", "").split(',')
            val left = leftRight[0].trim()
            val right = leftRight[1].trim()
            map[coordinate] = listOf(left, right)
            if (currentPosition == "") {
                currentPosition = coordinate
            }
        }
    }
    fun gcd(a: Long, b: Long): Long {
        return if (b == 0L) a else gcd(b, a % b)
    }

    fun lcm(a: Long, b: Long): Long {
        return (a * b) / gcd(a, b)
    }

    fun playGame2(gameFile : File): Long {
        val lines = gameFile.readLines()
        val steps = lines.get(0).trim()
        val ghosts = mutableListOf<Ghost>()
        val coordinates = mutableListOf<Coordinate>()

        lines.slice(2..<lines.size)
            .forEach { line ->
                val coordStep = line.split('=')
                val position = coordStep[0].trim()
                val leftRight = coordStep[1].trim().replace("(", "").replace(")", "").split(',')
                val left = leftRight[0].trim()
                val right = leftRight[1].trim()
                val coordinate = Coordinate(position, left, right)
                coordinates.add(coordinate)
                if (position.endsWith("A")) {
                    ghosts.add(Ghost(coordinate))
                }
            }

        var index = 0
        var totalSteps = 0
        val maxLength = steps.length
        while (ghosts.any { g -> g.stepsToZ == 0 }) {
            totalSteps++

            if (index == maxLength) index = 0;
            val moveLeft = steps[index] == 'L'
            val ghostNextPositions = ghosts.map { g -> if (moveLeft) g.position.left else g.position.right }
            val positions = coordinates.filter { p ->
                ghostNextPositions.contains(p.coordinate)
            }

            ghosts.filter { g -> g.stepsToZ == 0 }.forEach { g ->
                val c = positions.first { c ->
                    c.coordinate == if (moveLeft) g.position.left else g.position.right
                }
                if (c.coordinate.endsWith("Z")) {
                    g.stepsToZ = totalSteps;
                } else { // keep looking
                    g.position = c
                }
            }
            index++
        }

        val numbers : List<Long> = ghosts.map { g -> g.stepsToZ.toLong() }.sorted()
        var result = numbers[0]
        for (i in 1 until numbers.size) {
            result = lcm(result, numbers[i])
        }
        return result
    }

    fun playGame(): Int {
        var steps = 0
        while(true) {
            for (i in 0..instructions.length) {
                if (i == instructions.length || currentPosition == goal) {
                    break
                }
                steps += 1
                val index = if (instructions[i] == 'L') 0 else 1
                currentPosition = map[currentPosition]!![index]
                if (currentPosition == goal)
                    return steps
            }
        }
    }
}