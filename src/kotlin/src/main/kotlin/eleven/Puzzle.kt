package eleven

import java.io.File
import kotlin.math.abs

class Puzzle {
    data class Point (val x: Long, val y: Long)
    fun playGame(gameFile: File, delta: Long) {
        val lines = gameFile.readLines()
        val columns = mutableListOf<Int>()
        columns.addAll((0 until lines[0].length).toList())
        val rows = mutableListOf<Int>()
        rows.addAll(lines.indices.toList())

        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c == '#') {
                    rows.remove(y)
                    columns.remove(x)
                }
            }
        }

        val points = mutableListOf<Point>()
        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c == '#') {
                    val nY  = y + (rows.count { i -> i < y } * delta) - rows.count { i -> i < y }
                    val nX  = x + (columns.count { i -> i < x } * delta) - columns.count { i -> i < x }
                    points.add(Point(nX, nY))
                }
            }
        }

        val matched = HashMap<Point, List<Point>>()
        var total: Long = 0;
        points.forEach { point ->
            points.filter { p -> p != point }.forEach { other ->
                if (matched[other]?.contains(point) != true){
                    total += abs(point.x - other.x) + abs(point.y - other.y)
                    matched[other] = if (matched[other] == null) listOf(point) else matched[other]!!.plus(point)
                    matched[point] = if (matched[point] == null) listOf(other) else matched[point]!!.plus(other)
                }
            }
        }
        println("total: ${total}")
    }
}