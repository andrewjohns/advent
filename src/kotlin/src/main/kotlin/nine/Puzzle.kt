package nine

import java.io.File



class Puzzle {

    fun playGame(gameFile: File) {
        val lines = gameFile.readLines()

        var sum = 0;

        lines.forEach { line ->
            var numbers = line.split(' ').map { s -> s.toInt() }
            val sequence = mutableListOf<List<Int>>()
            sequence.add(numbers)

            while (true) {
                val nextSequence = mutableListOf<Int>()
                for (i in 0 until numbers.size - 1) {
                    val first = numbers[i]
                    val second = numbers[i + 1]
                    nextSequence.add(second - first)
                }

                sequence.add(nextSequence)

                if (nextSequence.all { n -> n == 0 }) {
                    break
                } else {
                    numbers = nextSequence
                }
            }

            sequence.reverse()

            var lastNumber = 0
            for (i in 1 until sequence.size) {
                val highest = sequence[i].last()
                lastNumber = lastNumber + highest
            }

            sum += lastNumber
        }

        println(sum)
    }

    fun playGame2(gameFile: File) {
        val lines = gameFile.readLines()

        var sum = 0;

        lines.forEach { line ->
            var numbers = line.split(' ').map { s -> s.toInt() }
            val sequence = mutableListOf<List<Int>>()
            sequence.add(numbers)

            while (true) {
                val nextSequence = mutableListOf<Int>()
                for (i in 0 until numbers.size - 1) {
                    val first = numbers[i]
                    val second = numbers[i + 1]
                    nextSequence.add(second - first)
                }

                sequence.add(nextSequence)

                if (nextSequence.all { n -> n == 0 }) {
                    break
                } else {
                    numbers = nextSequence
                }
            }

            sequence.reverse()

            var lastNumber = 0
            for (i in 1 until sequence.size) {
                val highest = sequence[i].first()
                lastNumber = highest - lastNumber
            }

            sum += lastNumber
        }

        println(sum)
    }
}