package ten

import java.io.File

data class Point(val x: Int, val y: Int);

class Puzzle {

    fun playGame(gameFile: File) {
        val gameBoard = mutableListOf<List<Char>>()

        var startRow: Int= 0;
        var startPoint = 0;

        gameFile.readLines().forEachIndexed { index, line ->
            val row = mutableListOf<Char>();
            line.forEachIndexed { charIndex, c ->
                if (c == 'S') {
                    startRow = index;
                    startPoint = charIndex
                }
                row.add(c)
            }
            gameBoard.add(row)
        }

        val start = Point(startPoint, startRow)

        val left = Point(-1,0)
        val right = Point(1,0)
        val up = Point(0,1)
        val down = Point(0,-1)
        val startingDirections = listOf(down)

        startingDirections.forEach { d ->
            try {
                newStart(gameBoard, start, d)
            } catch (e: Exception) {
                println(e)
            }
        }
    }

    // Up 1 | Down -1
    // Right 1 | Left -1
    private fun nextDelta(pipe: Char, p: Point): Point? {
        when(pipe) {
            '|' -> {
                if (p.y == -1) return Point(0,-1)
                if (p.y == 1) return Point(0,1)
            }
            '-' -> {
                if (p.x == -1) return Point(-1,0)
                if (p.x == 1) return Point(1,0)
            }
            'L' -> {
                if (p.x == -1) return Point(0,1)
                if (p.y == -1) return Point(1, 0)
            }
            'J' -> {
                if (p.x == 1) return Point(0, 1)
                if (p.y == -1) return Point(-1,0)
            }
            '7' -> {
                if (p.x == 1) return Point(0,-1)
                if (p.y == 1) return Point(-1, 0)
            }
            'F' -> {
                if (p.x == -1) return Point(0,-1)
                if (p.y == 1) return Point(1, 0)
            }
        }
        return null;
    }

    private fun newStart(gameBoard: List<List<Char>>, start: Point, iDelta: Point)  {

        val points = mutableListOf<Point>()
        points.add(start)

        var char = '.'
        var location = start
        var delta : Point? = iDelta
        while (char != 'S') {
            location = Point(location.x + delta!!.x, location.y - delta.y)
            char = gameBoard[location.y][location.x]
            points.add(location)

            if (char != 'S') {
                delta = nextDelta(char, delta!!)
                if (delta == null) return
            }
        }

        var sum = 0;
        gameBoard.forEachIndexed { y, chars ->
            chars.forEachIndexed { x, char ->
                if (! points.any { p -> p.x == x && p.y == y }) {
                    val isInside = isPointInPath(Point(x,y), points)
                    if (isInside) sum++
                }
            }
        }
        val distance = (points.size - 1)/2
        println("Distance: ${distance}")
        println("Total: ${sum}")
    }

    private fun isPointInPath(p: Point, path: List<Point>): Boolean {
        var isInside = false
        var j = path.size - 1

        for (i in path.indices) {
            val pi = path[i]
            val pj = path[j]

            if (
                (pi.y > p.y) != (pj.y > p.y)
                && (p.x < (pj.x - pi.x)
                        * (p.y - pi.y)
                        / (pj.y - pi.y)
                        + pi.x)
            ) {
                isInside = !isInside
            }
            j = i
        }

        return isInside
    }
}