package twelve

import java.io.File
class Puzzle {
    data class configNum(val config: String, val num: List<Int>)
    private val map =  HashMap<configNum, Long>()

    fun count(config: String, numbers: List<Int>): Long {
        if (config.isEmpty()) {
            return if (numbers.isEmpty()) 1 else 0
        }
        if (numbers.isEmpty()) {
            return if (config.contains('#')) 0 else 1
        }
        val key = configNum(config, numbers)
        if (map.containsKey(key)) {
            return map[key]!!;
        }

        var result = 0L
        if (".?".contains(config[0])) {
            result += count(config.substring(1), numbers.subList(0, numbers.size))
        }

        if ("#?".contains(config[0])) {

            if (numbers[0] <= config.length
                && !config.substring(0, numbers[0]).contains(".")
                && (numbers[0] == config.length || config[numbers[0]] != '#')
            ) {
                val newNumbers = numbers.subList(1, numbers.size)
                val configSubString = numbers[0] + 1
                if (configSubString > config.length) {
                    result += count("", newNumbers)
                } else {
                    val newConfig = config.substring(numbers[0] + 1)
                    result += count(newConfig, newNumbers)
                }

            } else {
                result += 0
            }
        }
        map[key] = result
        return result;
    }

    fun playGame(gameFile: File, repeat: Int) {

        var total = 0L;

        gameFile.readLines().forEachIndexed { index, line ->

            val configNumbers = line.split(" ")
            val configList = mutableListOf<String>()
            val numberList = mutableListOf<String>()
            val config = configNumbers[0]
            val numbers = configNumbers[1]

            repeat(repeat) {
                configList.add(config)
                numberList.add(numbers)
            }

            val num = numberList.joinToString(",").split(",").map { it.toInt() };
            val cnf = configList.joinToString("?")
            total += count(cnf, num)
        }
        println("total: ${total}")
    }
}