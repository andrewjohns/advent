import fs from 'fs';
import {Component, ISeedRange} from "./structure";

const data = fs.readFileSync('./src/five/five.txt').toString();

const seedsExp = /seeds:\s(?<cap>([0-9\s]|\n)*)/gm;
const seedToSoilExp = /seed-to-soil map:\n(?<cap>([0-9\s]|\n)*)/gm;
const soilToFertilizerExp = /soil-to-fertilizer map:\n(?<cap>([0-9\s]|\n)*)/gm;
const fertilizerToWaterExp = /fertilizer-to-water map:\n(?<cap>([0-9\s]|\n)*)/gm;
const waterToLightExp = /water-to-light map:\n(?<cap>([0-9\s]|\n)*)/gm;
const lightToTemperatureExp = /light-to-temperature map:\n(?<cap>([0-9\s]|\n)*)/gm;
const temperatureToHumidityExp = /temperature-to-humidity map:\n(?<cap>([0-9\s]|\n)*)/gm;
const humidityToLocationExp = /humidity-to-location map:\n(?<cap>([0-9\s]|\n)*)/gm;

const seedsResult = (seedsExp.exec(data)?.groups ?? null)?.cap ?? null;
const seedToSoilResult = (seedToSoilExp.exec(data)?.groups ?? null)?.cap ?? null;
const soilToFertilizerResult = (soilToFertilizerExp.exec(data)?.groups ?? null)?.cap ?? null;
const fertilizerToWaterResult = (fertilizerToWaterExp.exec(data)?.groups ?? null)?.cap ?? null;
const waterToLightResult = (waterToLightExp.exec(data)?.groups ?? null)?.cap ?? null;
const lightToTemperatureResult = (lightToTemperatureExp.exec(data)?.groups ?? null)?.cap ?? null;
const temperatureToHumidityResult = (temperatureToHumidityExp.exec(data)?.groups ?? null)?.cap ?? null;
const humidityToLocationResult = (humidityToLocationExp.exec(data)?.groups ?? null)?.cap ?? null;

const parseSeeds = (str: string | null, arr: number[]) => {
    if (str === null) return;

    let seedArray = str.trim().split(' ');
    for(let i= 0; i < seedArray.length; i++ ) {
        try {
            arr.push(parseInt(seedArray[i]));
        } catch (error) {
            console.error(error);
        }
    }
}

const parseSeeds2 = (str: string | null, arr:  ISeedRange[]) => {
    if (str === null) return;

    let seedArray = str.trim().split(' ');
    for(let i= 0; i < seedArray.length; i++ ) {
        try {
            let seedStart = parseInt(seedArray[i]);
            i++;
            let range = parseInt(seedArray[i]);
            arr.push( {
                start: seedStart,
                length: range
            });
        } catch (error) {
            console.error(error);
        }
    }
}

const findLowestLocation = (seeds: number[], startComponent: Component) => {
    let location : number | null = null;

    seeds.forEach(n => {
        let seedLocation = startComponent.findLowest(n);
        if (location == null || location > seedLocation) {
            location = seedLocation;
        }
    });

    return location;
}

const findLowestLocation2 = (seeds: ISeedRange[], startComponent: Component) => {
    let location : number | null = null;

    seeds.forEach(n => {
        for(let x= n.start; x < (n.start + n.length -1); x++) {
            let seedLocation = startComponent.findLowest(x);
            if (location == null || location > seedLocation) {
                location = seedLocation;
            }
        }
    });
    return location;
}

const seeds: number[] = [];
parseSeeds(seedsResult, seeds);

const seedToSoil = new Component(seedToSoilResult, 'seedToSoil');
const soilToFertilizer= new Component(soilToFertilizerResult, 'soilToFertilizer');
const fertilizerToWater = new Component(fertilizerToWaterResult, 'fertilizerToWater');
const waterToLight = new Component(waterToLightResult, 'waterToLight');
const lightToTemperature = new Component(lightToTemperatureResult, 'lightToTemperature');
const temperatureToHumidity = new Component(temperatureToHumidityResult, 'temperatureToHumidity');
const humidityToLocation = new Component(humidityToLocationResult, 'humidityToLocation');

seedToSoil.setNextComponent(soilToFertilizer);
soilToFertilizer.setNextComponent(fertilizerToWater);
fertilizerToWater.setNextComponent(waterToLight);
waterToLight.setNextComponent(lightToTemperature);
lightToTemperature.setNextComponent(temperatureToHumidity);
temperatureToHumidity.setNextComponent(humidityToLocation);

let location = findLowestLocation(seeds, seedToSoil);
console.log('location', location);

const seedRanges: ISeedRange[] = [];
parseSeeds2(seedsResult, seedRanges);
//location = findLowestLocation2(seedRanges, seedToSoil);
console.log('location', location);
