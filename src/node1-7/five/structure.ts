export interface IRangeItem {
    destinationRangeStart: number,
    sourceRangeStart: number,
    rangeLength: number,
}

export interface ISeedRange {
    start: number,
    length: number,
}

export interface IRange {
    ranges: IRangeItem[]
}

export class Component {

    private _name: string;
    private _ranges: IRangeItem[];
    private _nextComponent: Component | null = null;

    constructor(builder: string | null, name: string) {

        if (builder === null) throw new Error("Invalid Input");

        const ranges: IRangeItem[] = [];

        builder.trim().split('\n').forEach(l => {
            const item = l.trim().split(' ');
            if (item.length < 3) {
                throw new Error("Invalid Input");
            }
            ranges.push( {
                destinationRangeStart: parseInt(item[0]),
                sourceRangeStart: parseInt(item[1]),
                rangeLength: parseInt(item[2]),
            });
        });

        this._name = name;
        this._ranges = ranges;
    }

    setNextComponent(component: Component) {
        this._nextComponent = component;
    }

    findLowest(input: number): number {
        let output: number | null = null;

        for(let i = 0; i < this._ranges.length; i++) {
            let item = this._ranges[i];
            let minSource = item.sourceRangeStart;
            let rangeLimit = item.rangeLength - 1;
            let maxSource = minSource + rangeLimit;
            // in range
            if (input >= minSource && input <= maxSource ) {
                let offset = input - minSource;
                output = item.destinationRangeStart + offset;
                break;
            }
        }

        if (this._nextComponent !== null) {
            return this._nextComponent.findLowest(output ?? input);
        } else {
            return output ?? input;
        }
    }
}
