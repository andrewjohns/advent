export interface ICard {
    index: number
    winningNumbers: number,
    cards: ICard[]
}


export class Helper {

    private _numberExp = /[0-9]+/g;
    private _cards : ICard[] = [];

    getCards = (): ICard[] => {
        return this._cards;
    }

    calculateWinningValue = (num: number) : number => {
        //return Math.floor(Math.pow(2, num - 1));
        if (num < 1) {
            return 0;
        } else if (num === 1) {
            return 1;
        }
        let value = 1;
        for(let i = 0; i < num - 1; i++) {
            value = value * 2;
        }

        return value;
    }

    calculateWinningNumbers = (gameNumbers:  number[], winningNumbers:  number[]) => {
        let winningNumberCount = 0;
        winningNumbers.forEach(w => {
            for(let n=0; n < gameNumbers.length; n++) {
                if (w === gameNumbers[n]) {
                    winningNumberCount++;
                    break;
                }
            }
        });
        return winningNumberCount;
    }

    getNumbers = (str: string) : number[] => {
        let numbers : number[] = []
        let match : RegExpExecArray | null;
        while ((match = this._numberExp.exec(str)) !== null) {
            numbers.push(parseInt(match.toString()));
        }
        return numbers
    }

    returnWinnings = () => {
        for(let c=0; c < this._cards.length; c++) {
            let card = this._cards[c];
            if (card.winningNumbers < 1) {
                continue;
            }

            let minIndex = card.index + 1;
            let maxIndex = minIndex + (card.winningNumbers - 1);

            let childCards = this._cards.filter(f => f.index >= minIndex && f.index <= maxIndex);
            card.cards.push( ... childCards);
        }
    }

    setGame = (data: string) : number => {
        let sum = 0;
        let lines = data.split("\n");
        lines.forEach((line, index) => {
            let gameCard = line.trim().split('|');
            let winningNumbers = this.getNumbers(gameCard[0].split(':')[1]);
            let gameNumbers = this.getNumbers(gameCard[1]);
            let numbers = this.calculateWinningNumbers(gameNumbers, winningNumbers);

            this._cards.push( {
                index,
                winningNumbers: numbers,
                cards: []
            });
            let value = this.calculateWinningValue(numbers);
            sum += value;
        });
        this.returnWinnings()
        return sum;
    }

    calculateTotalCards = (cards: ICard[])  : number => {
        let sum = cards.length;
        cards.forEach(c => {
            sum += this.calculateTotalCards(c.cards);
        });
        return sum;
    }
}