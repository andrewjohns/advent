import fs from 'fs';

const data = fs.readFileSync('./src/one/one.txt').toString();
const lines = data.split("\n");
let sum = 0;

let digits = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

interface indexValue {
    index: number,
    value: number,
    replacementString: string | null
}

const digitExp = /\d/g

console.log()

const searchInLine = (line: string, expression: RegExp, replacementString : string | null = null, replacementValue : number | null = null) => {
    let match : RegExpExecArray | null;
    const searchIndexes : indexValue[] = [];
    while ((match = expression.exec(line)) !== null) {
        searchIndexes.push({
            index: match.index,
            value: replacementValue ?? parseInt(match.toString()),
            replacementString
        });
    }
    return searchIndexes;
}


const compare = ( a : indexValue, b : indexValue ) => {
    if ( a.index < b.index ){
        return -1;
    }
    else if ( a.index > b.index ){
        return 1;
    } else {
        return 0;
    }
}

lines.forEach(l => {
    const input = l;
    const output : indexValue[] = [];
    digits.forEach((value, index) => {
        let o = searchInLine(l, new RegExp(`${value}`, 'g'), value, index+1);
        output.push(... o);
    });

    let digitItems = searchInLine(l, digitExp);
    output.push(... digitItems)
    output.sort( compare );
    let firstLast = `${output[0].value}${output[output.length - 1].value}`
    sum += parseInt(firstLast);
});

console.log(sum);


