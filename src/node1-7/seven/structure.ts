import cardRank from "./global"


export interface Breakdown {
    letter: string,
    count: number
}

export interface Hand {
    hand: string,
    breakdown: Breakdown[],
    points: number,
    score: number,
}
export class Helper {

    private _hands: Hand[] = []

    private _winningHands = [
        {
            count: [5],
            score: 7
        },
        {
            count: [4],
            score: 6
        },
        {
            count: [3,2],
            score: 5
        },
        {
            count: [3],
            score: 4
        },
        {
            count: [2,2],
            score: 3
        },
        {
            count: [2],
            score: 2
        },
        {
            count: [1],
            score: 1
        },
    ];



    compareHands( a : Hand, b : Hand )  {
        if ( a.score > b.score ){
            return -1;
        }
        else if ( a.score < b.score ){
            return 1;
        } else {
            for(let i =0; i < 5; i++) {
                let aValue = cardRank(a.hand[i]);
                let bValue = cardRank(b.hand[i]);
                if ( aValue > bValue ){
                    return -1;
                }
                else if ( aValue < bValue ){
                    return 1;
                }
            }
            return 0;
        }
    }

    determineScore() {
        this._hands.forEach(hand => {
            for(let w = 0; w < this._winningHands.length; w++) {
                let winningHand = this._winningHands[w];
                let hasWinningHand = true;
                let testedLetter: string | null = null;
                let jokerCount = hand.breakdown.find(b => b.letter === "J")?.count ?? 0;
                if (jokerCount === 5) {
                    hand.score = 7;
                    break;
                }
                for(let c = 0; c < winningHand.count.length; c++) {
                    let count = winningHand.count[c];
                    let found = hand.breakdown.find(b =>
                        ((b.count + jokerCount) >= count && b.letter !== "J")
                        && (testedLetter === null || b.letter !== testedLetter)
                    ) ?? null;

                    if (found !== null) {
                        let newCount = jokerCount - (count - found.count);
                        if (newCount !== jokerCount) {
                            console.log(hand.hand);
                            jokerCount = newCount;
                        }

                        testedLetter = found.letter;
                    } else {
                        hasWinningHand = false;
                        break;
                    }
                }

                if (hasWinningHand) {
                    hand.score = winningHand.score;
                    break;
                }
            }
        });
    }

    getHands() {
        return this._hands;
    }

    breakdownHand(str: string) {
        let breakdown : Breakdown[] = [];
        const sortedStr = str.split('').sort().join('');
        for (let c=0; c < sortedStr.length; c++) {
            let current = breakdown.find(b => b.letter === sortedStr[c]) ?? null;
            if (current === null) {
                breakdown.push({
                    letter: sortedStr[c],
                    count: 1
                });
            } else {
                current.count = current.count + 1;
            }
        }

        return breakdown;
    }

    setGame = (data: string)  => {
        let lines = data.split("\n");
        lines.forEach(line => {
            let handScore = line.split(' ');
            let breakdown = this.breakdownHand(handScore[0]);
            let points = parseInt(handScore[1]);
            this._hands.push({
                hand: handScore[0],
                breakdown,
                points,
                score: 0
            });
        });
    }
    playGame()  {
        this.determineScore();
        let total = 0
        this._hands.sort( this.compareHands ).reverse().forEach((hand, index) => {
            console.log(hand.hand, hand.points, hand.score)
            total += (hand.points * (index + 1));
        })

        return total;
    }

}