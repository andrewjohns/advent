export interface TimeDistance {
    time: number,
    distance: number
}

export interface Wins {
    timeDistance: TimeDistance,
    wins: number
}


export class Helper {

    private _numberExp = /[0-9]+/g;
    private _timeDistance: TimeDistance[] = [];

    getTimeDistance = () => {
        return this._timeDistance;
    }
    getNumbers(str: string) : number[]  {
        let numbers : number[] = []
        let match : RegExpExecArray | null;
        while ((match = this._numberExp.exec(str)) !== null) {
            numbers.push(parseInt(match.toString()));
        }
        return numbers
    }
    setGame = (data: string)  => {
        let sum = 0;
        let lines = data.split("\n");
        let time : number[] = [];
        let distance : number[] = [];
        lines.forEach((line, index) => {
            let game = line.trim().split(':');
            let numbers = this.getNumbers(game[1]);

            if (index === 0) {
                time.push(... numbers);
            } else {
                distance.push(... numbers);
            }
        });

        time.forEach((time, index) => {
            this._timeDistance.push({
                time,
                distance: distance[index]
            });
        });
    }

    setGame2 = (data: string)  => {
        let sum = 0;
        let lines = data.split("\n");
        let time : number[] = [];
        let distance : number[] = [];
        lines.forEach((line, index) => {
            let game = line.trim().split(':');
            let number = parseInt(this.getNumbers(game[1]).join(""));

            if (index === 0) {
                time.push(number);
            } else {
                distance.push(number);
            }
        });

        time.forEach((time, index) => {
            this._timeDistance.push({
                time,
                distance: distance[index]
            });
        });
    }

    playGame() : Wins[] {
        let wins: Wins[] = []
        this._timeDistance.forEach(td => {
            let round = {
                timeDistance: td,
                wins: 0
            }
            wins.push(round);
            for(let i= 1; i < td.time; i++) {
                let timeLeft = td.time - i;
                let distance = timeLeft * i;
                let win = distance > td.distance;
                if (win) {
                    round.wins++;
                }

                if (round.wins > 0 && win === false) {
                    break;
                }
            }
        });

        return wins;
    }
}