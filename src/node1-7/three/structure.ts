import {setEngine} from "crypto";

export interface INumber {
    index: number;
    value: number,
    length: number,
    isEnginePart: boolean
}

export class Helper {

    private _numbers: Map<number, INumber[]> = new Map<number, INumber[]>();
    private _symbols: Map<number, number[]> = new Map<number, number[]>();
    private _gears: Map<number, number[]> = new Map<number, number[]>();

    private _numberExp = /[0-9]+/g;
    private _symbolExp = /[^A-z0-9.]/g;
    private _gearExp = /[*]/g;

    getNumbers = () : Map<number, INumber[]> => {
        return this._numbers;
    }

    getSymbols = () : Map<number, number[]> => {
        return this._symbols;
    }

    getGears = () : Map<number, number[]> => {
        return this._gears;
    }

    addNumbers = (index: number, str: string) => {
        let match : RegExpExecArray | null;
        while ((match = this._numberExp.exec(str)) !== null) {
            let matchedValue = match.toString();
            let currentLineNumbers = this._numbers.get(index) ?? null;
            let number: INumber = {
                index: match.index,
                value: parseInt(matchedValue),
                length: matchedValue.length,
                isEnginePart: false
            };

            if (currentLineNumbers === null) {
                this._numbers.set(index, [number]);
            } else {
                currentLineNumbers.push(number)
            }
        }
    }

    addSymbols = (index: number, str: string) => {
        let match : RegExpExecArray | null;
        while ((match = this._symbolExp.exec(str)) !== null) {
            let currentLineSymbols = this._symbols.get(index) ?? null;
            if (currentLineSymbols === null) {
                this._symbols.set(index, [match.index]);
            } else {
                currentLineSymbols.push(match.index)
            }
        }
    }

    addGears = (index: number, str: string) => {
        let match : RegExpExecArray | null;
        while ((match = this._gearExp.exec(str)) !== null) {
            let currentLineGears = this._gears.get(index) ?? null;
            if (currentLineGears === null) {
                this._gears.set(index, [match.index]);
            } else {
                currentLineGears.push(match.index)
            }
        }
    }

    symbolMatched = (indexes: number[], positions: number[]) : boolean => {
        let isMatched = false;
        indexes.forEach(i => {
            let symbolIndex = this._symbols.get(i) ?? null;
            if (symbolIndex !== null) {
                positions.forEach(p => {
                    // @ts-ignore
                    if (symbolIndex.indexOf(p) > -1) {
                        isMatched = true;
                        return true;
                    }
                });
            }

            if (isMatched) {
                return true;
            }
        });
        return isMatched;
    }

    setEnginePart = () => {
        this._numbers.forEach((value, index) => {
            value.forEach(n => {
                let positions: number[] = [];
                let indexes: number[] = [];
                let iStart = index > 0 ? index - 1 : 0;
                let iEnd = index + 1;
                let pStart = n.index > 0 ? n.index - 1 : 0;
                let pEnd = (n.index + n.length) + 1;

                for(let p= pStart; p < pEnd; p++) {
                    positions.push(p);
                }

                for(let i= iStart; i <= iEnd; i++) {
                    indexes.push(i);
                }

                n.isEnginePart = this.symbolMatched(indexes, positions);
            });
        });
    }

    calculateEngineParts = () => {
        let sum = 0;
        this._numbers.forEach((numbers, index) => {
            numbers.forEach(n => {
                if (n.isEnginePart) {
                    sum += n.value;
                }
            })
        });

        return sum;
    }

    calculateGearRatio = () : number => {
        let sum = 0;
        this._gears.forEach((gearIndexes, lineIndex) => {
            let numbers: INumber[] = [];
            let iStart = lineIndex > 0 ? lineIndex - 1 : 0;
            let iEnd = lineIndex + 1;
            for(let i= iStart; i <= iEnd; i++) {
                let numberLine = this._numbers.get(i) ?? null;
                if (null !== numberLine) {
                    // @ts-ignore
                    numbers.push(... numberLine);
                }
            }

            gearIndexes.forEach(i => {
                let gearNumbers: number[] = [];
                numbers.forEach(n => {
                    let left = n.index > 0 ? n.index - 1 : 0;
                    let right = n.index + n.length;

                    if (left <= i && i <= right) {
                        gearNumbers.push(n.value);
                    }
                });

                if (gearNumbers.length == 2) {
                    sum += (gearNumbers[0] * gearNumbers[1])
                }
            });
        });

        return sum;
    }


    setGame = (data: string) => {
        let lines = data.split("\n");
        lines.forEach((value, index) => {
            this.addNumbers(index, value)
            this.addSymbols(index, value);
            this.addGears(index, value);
        });

        this.setEnginePart();
    }



}