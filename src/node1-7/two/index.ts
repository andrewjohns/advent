import fs from 'fs';
import {Helper, IGame} from "./structure";

const data = fs.readFileSync('./src/two/two.txt').toString();
const helper = new Helper();
helper.setGames(data);

// RED, GREEN, BLUE
const answer = "12 13 14".split(' ');



console.log("--------------", answer, "--------------");
const colours = new Map<string, number>();
// @ts-ignore
answer.forEach((value, index) => {
    switch (index) {
        case 0:
            colours.set('red', parseInt(value));
            break;
        case 1:
            colours.set('green', parseInt(value));
            break;
        case 2:
            colours.set('blue', parseInt(value));
            break;
    }

});

let winningGames: IGame[] = helper.getWinningGames(colours);
console.log("========================");
let sum = 0;
let winningIds: number[] = [];
winningGames.forEach(g => {
    sum += g.game;
    winningIds.push(g.game);
})
// 2416
console.log(sum);
console.log("========================");
let power = 0;
helper.getGames()?.forEach(g => {
    power += g.power
});
console.log(power);



