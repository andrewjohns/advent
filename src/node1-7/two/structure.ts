export interface IGame {
    game: number
    colours: Map<string, number>,
    power: number
}

export class Helper {

    private _games : IGame[] | null = null;

    getGames = () => {
        return this._games;
    }

    getWinningGames = (colours: Map<string, number>) => {
        const winningGames: IGame[] = [];
        if (this._games === null) {
            return winningGames;
        }

        this._games.forEach(game => {
           let valid = true;
            colours.forEach((value, key) => {
                valid = valid && (game.colours.get(key) ?? 0) <= value;
           });

            if (valid) {
                winningGames.push(game);
            }
        });

        return winningGames;
    }

    setPower = () => {
        this.getGames()?.forEach(g => {
           let power = 1;
           g.colours.forEach((value) => {
               power = power * value;
           })
            g.power = power;
        });
    }

    setGames = (data: string) => {
        const gameExp = /Game\s*(?<cap>[0-9]*)/;
        const games: IGame[] = []
        const lines = data.split('\n');

        lines.forEach(line => {
            let gameRounds = line.trim().split(':');
            let game =  (gameExp.exec(gameRounds[0])?.groups ?? null)?.cap ?? null;

            if (null === game) {
                return;
            }

            let newGame : IGame = {
                game: parseInt(game),
                colours: new Map<string, number>(),
                power: 0
            }
            games.push(newGame);


            let rounds = gameRounds[1].split(';')
            rounds.forEach(round => {
                let coloursCount = round.split(',');
                coloursCount.forEach(colourCount => {
                    let countColour = colourCount.trim().split(' ');
                    let count = parseInt(countColour[0]);
                    let colour = countColour[1].toLowerCase();
                    if (colour === null) {
                        console.log("missing colour", colour);
                        return;
                    }
                    let currentValue = newGame.colours.get(colour) ?? null;
                    if (currentValue === null || currentValue < count) {
                        newGame.colours.set(colour, count);
                    }
                });
            });
        });
        this._games = games;
        this.setPower();
    }

}