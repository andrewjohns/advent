boxes = []


def get_data(f):
    with open(f, 'r') as file:
        return file.read()


def my_hash(group):
    hash_value = 0
    for c in list(group):
        hash_value = (hash_value + ord(c)) * 17 % 256
    return hash_value


def do_equal(key):
    details = key.split('=')
    label_prefix = details[0]
    value = details[1]
    label = label_prefix + " " + value
    box = my_hash(label_prefix)
    current_items = boxes[box]
    added = False
    for idx, removable in enumerate(current_items):
        if removable.startswith(label_prefix):
            current_items.remove(removable)
            current_items.insert(idx, label)
            added = True
    if not added:
        current_items.append(label)
    boxes[box] = current_items


def do_minus(key):
    details = key.split('-')
    label_prefix = details[0]
    box = my_hash(label_prefix)
    current_items = boxes[box]
    for removable in current_items:
        if removable.startswith(label_prefix):
            current_items.remove(removable)


def calculate_focus_power():
    power = 0
    for boxi, box in enumerate(boxes):
        for leni, lens in enumerate(box):
            focus_power = int(lens.split(' ')[1])
            power = power + (boxi + 1) * (leni + 1) * focus_power
    return power


data = get_data('data.txt')
parts = data.split(',')
total = 0
total_focus_power = 0

for i in range(0, 256):
    boxes.append([])

for part in parts:
    total = total + my_hash(part)
    if '=' in part:
        do_equal(part)
    else:
        do_minus(part)

total_focus_power = calculate_focus_power()
print("total", total)
print("total_focus_power", total_focus_power)
