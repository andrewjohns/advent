def get_data(f):
    with open(f, 'r') as file:
        return file.read()


def move_rocks(rows):
    rows.reverse()
    for i in range(1, len(rows)):
        current = rows[i-1]
        below = rows[i:]
        for idx, value in enumerate(current):
            for p in range(len(below)-1, 0, - 1):
                top = below[p][idx]
                bottom = below[p - 1][idx]
                if bottom == 'O' and top == '.':
                    below[p][idx] = 'O'
                    below[p - 1][idx] = '.'
            if value == 'O':
                top = below[0][idx]
                row = current[idx]
                if row == 'O' and top == '.':
                    below[0][idx] = 'O'
                    current[idx] = '.'
    rows.reverse()
    return rows

def count(rows):
    sum = 0
    for idx, row in enumerate(rows):
        sum = sum + (row.count("O") * (len(rows) - idx))

    return sum


data = get_data('data.txt')
game = [list(row) for row in data.splitlines()]

game = move_rocks(game)
print(count(game))


game = [list(row) for row in data.splitlines()]

cache = {}
found = False
for x in range(0, 1000000000):
    for i in range(0, 4):
        if i == 0:
            game = move_rocks(game)
        else:
            game = [list(x) for x in zip(*game[::-1])]
            game = move_rocks(game)
    game = [list(x) for x in zip(*game[::-1])]
    key = hash(''.join([''.join(g) for g in game]))

    if key in cache.keys():
        val = x - cache.get(key)[0]
        if (1000000000 - x - 1) % val == 0:
            break
    else:
        cache[key] = [x]

print(count(game))




