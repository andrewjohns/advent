def get_data(f):
    with open(f, 'r') as file:
        return file.read()

def get_value(rows: list[str]) -> int:
    for i in range(1, len(rows)):
        above = rows[:i][::-1]
        below = rows[i:]
        for a, b in zip(above, below):
            if a != b:
                break
        else:
            return i
    return 0


def get_smudge_value(rows: list[str]) -> int:
    for i in range(1, len(rows)):
        above = rows[:i][::-1]
        below = rows[i:]
        diffs = 0
        for a, b in zip(above, below):
            for x, y in zip(a, b):
                if x != y:
                    diffs += 1
            if diffs > 1:
                break
        if diffs == 1:
            return i
    return 0


total = 0
raw = get_data('test.txt')
games = raw.split("\n\n")
for game in games:
    rows = [list(row) for row in game.splitlines()]
    total += get_value(rows) * 100
    cols = list(zip(*rows))
    total += get_value(cols)

print("total A", total)

total = 0
for game in games:
    rows = [list(row) for row in game.splitlines()]
    total += get_smudge_value(rows) * 100
    cols = list(zip(*rows))
    total += get_smudge_value(cols)

print("total B", total)

